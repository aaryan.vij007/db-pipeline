#!/bin/bash
####


sed -i -e "s/Server_ip/$host/g" hilit_install.config
sed -i -e "s/Port/$port/g" hilit_install.config
sed -i -e "s/Superdb/$superdb/g" hilit_install.config
sed -i -e "s/Superusr/$superusr/g" hilit_install.config
sed -i -e "s/Spwd/$spwd/g" hilit_install.config
sed -i -e "s/Hltdbname/$hltdbname/g" hilit_install.config
sed -i -e "s/Hltcorepwd/$hltcorepwd/g" hilit_install.config
sed -i -e "s/Hltcmtpwd/$hltcmtpwd/g" hilit_install.config
sed -i -e "s/Hltpvtepwd/$hltpvtepwd/g" hilit_install.config
sed -i -e "s/Hlthdispwd/$hlthdispwd/g" hilit_install.config
sed -i -e "s/Hlthcodpwd/$hlthcodpwd/g" hilit_install.config
sed -i -e "s/Mdraschma/$mdraschma/g" hilit_install.config
sed -i -e "s/Hltsafqpwd/$hltsafqpwd/g" hilit_install.config
sed -i -e "s/Hltcapipwd/$hltcapipwd/g" hilit_install.config
sed -i -e "s/Hlthrscpwd/$hlthrscpwd/g" hilit_install.config
sed -i -e "s/Hltalatpwd/$hltalatpwd/g" hilit_install.config
sed -i -e "s/Hltsftypwd/$hltsftypwd/g" hilit_install.config
sed -i -e "s/Readonlyusrcapi/$readonlyusrcapi/g" hilit_install.config
sed -i -e "s/Hlttnt/$hlttnt/g" hilit_install.config
sed -i -e "s/Hltcln/$hltcln/g" hilit_install.config
sed -i -e "s/Hltanlytics/$hltanlytics/g" hilit_install.config
sed -i -e "s/Hltcmtanlytics/$hltcmtanlytics/g" hilit_install.config
sed -i -e "s/Hltcapianlytics/$hltcapianlytics/g" hilit_install.config
sed -i -e "s/Hltcoreanlytics/$hltcoreanlytics/g" hilit_install.config
sed -i -e "s/Hlthdisanlytics/$hlthdisanlytics/g" hilit_install.config
sed -i -e "s/Hltsafqanlytics/$hltsafqanlytics/g" hilit_install.config
sed -i -e "s/Hltdashbrd/$hltdashbrd/g" hilit_install.config
sed -i -e "s/Hltcoredashbrd/$hltcoredashbrd/g" hilit_install.config
sed -i -e "s/Hltcapidashbrd/$hltcapidashbrd/g" hilit_install.config
sed -i -e "s/Hlthdisdashbrd/$hlthdisdashbrd/g" hilit_install.config
sed -i -e "s/Hltcorereinst/$hltcorereinst/g" hilit_install.config
sed -i -e "s/Hltreinst/$hltreinst/g" hilit_install.config
sed -i -e "s/Hltarg/$hltarg/g" hilit_install.config
sed -i -e "s/Hiltargpwd/$hiltargpwd/g" hilit_install.config
sed -i -e "s/Orclfdw/$orclfdw/g" hilit_install.config
sed -i -e "s/Orcalefdwsvr/$orcalefdwsvr/g" hilit_install.config
sed -i -e "s/Pgagnt/$pgagnt/g" hilit_install.config
sed -i -e "s/Tagname/$tagname/g" hilit_install.config
sed -i -e "s/Hltcodelistsys/$hltcodelistsys/g" hilit_install.config
sed -i -e "s/Hltdropview/$hltdropview/g" hilit_install.config
sed -i -e "s/Hltcoreinst/$hltcoreinst/g" hilit_install.config
sed -i -e "s/Hltcamtinst/$hltcamtinst/g" hilit_install.config
sed -i -e "s/Hltpvtinst/$hltpvtinst/g" hilit_install.config
sed -i -e "s/Hlthdisinst/$hlthdisinst/g" hilit_install.config
sed -i -e "s/Hlthcodinst/$hlthcodinst/g" hilit_install.config
sed -i -e "s/Hltsafqinst/$hltsafqinst/g" hilit_install.config
sed -i -e "s/Hltcapiinst/$hltcapiinst/g" hilit_install.config
sed -i -e "s/Hltcamtinst/$hltcamtinst/g" hilit_install.config
sed -i -e "s/Hlthrscinst/$hlthrscinst/g" hilit_install.config
sed -i -e "s/Hltalatinst/$hltalatinst/g" hilit_install.config
sed -i -e "s/Hltsftyinst/$hltsftyinst/g" hilit_install.config

